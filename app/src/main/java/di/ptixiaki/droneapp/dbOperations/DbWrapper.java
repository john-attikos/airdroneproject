package di.ptixiaki.droneapp.dbOperations;

/**
 * Created by sinnaig on 27/4/2015.
 */


import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import di.ptixiaki.droneapp.debug.MyLogger;

public class DbWrapper extends SQLiteOpenHelper {

    private static final String DATABASE_NAME = "Android.db";
    private static final int DATABASE_VERSION = 26;

    public static final String USERS = "Users";
    public static final String USERS_USERNAME = "username";
    public static final String USERS_EMAIL = "email";
    public static final String USERS_PASSWORD = "password";
    public static final String USER_IS_LOGGEDIN = "loggedIn";

    public static final String ATTRIBUTES = "Attributes";
    public static final String ATTRIBUTES_NAME = "name";

    public static final String USERS_HAS_ATTRIBUTES = "Users_has_attributes";
    public static final String USERS_HAS_ATTRIBUTES_USER = "user";
    public static final String USERS_HAS_ATTRIBUTES_ATTRIBUTE = "attribute";

    public static final String HISTORY = "History";
    public static final String HISTORY_ID = "id";
    public static final String HISTORY_USERNAME = "username";
    public static final String HISTORY_ATTRIBUTE = "attribute";
    public static final String HISTORY_DATE = "date";
    public static final String HISTORY_VALUE = "value";

    /*** ANDROID CLIENT won't need Images table like server does
     public static final String IMAGES = "Attributes";
     public static final String IMAGES_NAME = "name";
     ***/


    private static final String CREATE_TABLE_USERS = "CREATE TABLE "
            + USERS+ "("
            + USERS_USERNAME + " TEXT PRIMARY KEY,  "
            + USERS_EMAIL + " TEXT, "
            + USERS_PASSWORD + " TEXT NOT NULL, "
            + USER_IS_LOGGEDIN + " TEXT " + ");";


    private static final String CREATE_TABLE_ATTRIBUTES = "CREATE TABLE "
            + ATTRIBUTES + "("
            + ATTRIBUTES_NAME + " TEXT PRIMARY KEY  " + ");";


    private static final String CREATE_TABLE_USERS_HAS_ATTRIBUTES = "CREATE TABLE "
            + USERS_HAS_ATTRIBUTES + "("
            + USERS_HAS_ATTRIBUTES_USER + " TEXT ,  "
            + USERS_HAS_ATTRIBUTES_ATTRIBUTE + " TEXT, "
            + " FOREIGN KEY(" +USERS_HAS_ATTRIBUTES_USER+ ") REFERENCES "+USERS+" ( " +USERS_USERNAME+ "), "
            + " FOREIGN KEY(" +USERS_HAS_ATTRIBUTES_ATTRIBUTE+ ") REFERENCES "+ATTRIBUTES+" ( " +ATTRIBUTES_NAME+ ") "
            + ");";

    private static final String CREATE_TABLE_HISTORY = "CREATE TABLE "
            + HISTORY + "("
            + HISTORY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
            + HISTORY_USERNAME + " TEXT, "
            + HISTORY_ATTRIBUTE + " TEXT, "
            + HISTORY_VALUE + " TEXT, "
            + HISTORY_DATE + " TEXT " + ");";



    public DbWrapper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        MyLogger.printi("Creating database");

        db.execSQL(CREATE_TABLE_USERS);
        db.execSQL(CREATE_TABLE_ATTRIBUTES);
        db.execSQL(CREATE_TABLE_USERS_HAS_ATTRIBUTES);
        db.execSQL(CREATE_TABLE_HISTORY);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        MyLogger.printi("Database on upgrade");

        db.execSQL("DROP TABLE IF EXISTS " + USERS);
        db.execSQL("DROP TABLE IF EXISTS " + USERS_HAS_ATTRIBUTES);
        db.execSQL("DROP TABLE IF EXISTS " + ATTRIBUTES);
        db.execSQL("DROP TABLE IF EXISTS " + HISTORY);

        onCreate(db);
    }

}

