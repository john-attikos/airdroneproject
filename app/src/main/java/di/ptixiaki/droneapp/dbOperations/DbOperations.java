package di.ptixiaki.droneapp.dbOperations;

/**
 * Created by sinnaig on 27/4/2015.
 */

import android.content.Context;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

import di.ptixiaki.droneapp.main.LoginActivity;


public class DbOperations {

    public UserOperations userOp = new UserOperations();
    public HistoryOperations historyOp = new HistoryOperations();
    public AttributesOperations attributesOp = new AttributesOperations();
    public UserHasAttributesOperations userHasAttributesOp = new UserHasAttributesOperations();


    // Database fields
    protected static DbWrapper dbHelper;
    protected static SQLiteDatabase database;

    public DbOperations(Context context) {
        dbHelper = new DbWrapper(context);
    }

    public void openDB() throws SQLException {
        database = dbHelper.getWritableDatabase();

        // Let one user stored by default
        LoginActivity.database.userOp.addUser("admin","admin","sdi0700308@di.uoa.gr");
    }

    public void closeDB() {
        dbHelper.close();
    }



}

