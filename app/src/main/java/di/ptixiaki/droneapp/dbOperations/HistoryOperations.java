package di.ptixiaki.droneapp.dbOperations;

import android.content.ContentValues;
import android.database.Cursor;
import android.util.Log;

import java.util.ArrayList;
import java.util.Calendar;

import di.ptixiaki.droneapp.dbObjects.History;
import di.ptixiaki.droneapp.debug.MyLogger;
import di.ptixiaki.droneapp.main.LoginActivity;

/**
 * Created by sinnaig on 4/5/2015.
 */

public class HistoryOperations {

    public int addHistory(String username, String attribute, String value, String ts) {

        // In case the attribute is missing, it is harmless to just add it.
        // (Note: it will be added only if it is missing)
        LoginActivity.database.attributesOp.addAttribute(attribute);

        // The timestamp will be generated in the server's side
        /*
        Long tsLong = System.currentTimeMillis();
        String ts = tsLong.toString();
        */
        String unit = new String();

        ContentValues values = new ContentValues();
        values.put(DbWrapper.HISTORY_USERNAME, username);
        values.put(DbWrapper.HISTORY_ATTRIBUTE, attribute);
        values.put(DbWrapper.HISTORY_VALUE, value+unit);
        values.put(DbWrapper.HISTORY_DATE, ts);
        long retval = DbOperations.database.insert(DbWrapper.HISTORY, null, values);
        if (retval < 0) {
            Log.e("", "+++HISTORY insertion to database failed!");
            return -1;
        }

        return 1;
    }


    /// for debugging
    public void printAllHistory() {
        MyLogger.printd("About to print all history");

        Cursor cursor = DbOperations.database.query(DbWrapper.HISTORY,
                new String[]{DbWrapper.HISTORY_ID, DbWrapper.HISTORY_USERNAME, DbWrapper.HISTORY_ATTRIBUTE, DbWrapper.HISTORY_VALUE, DbWrapper.HISTORY_DATE},
                null, null, null, null, null);
        cursor.moveToFirst();

        while(!cursor.isAfterLast()) {
            MyLogger.printd("\n ++++++++++++++++++");
            MyLogger.printd("id: "+cursor.getString(0));
            MyLogger.printd("user: " + cursor.getString(1));
            MyLogger.printd("attr: " + cursor.getString(2));
            MyLogger.printd("value: " + cursor.getString(3));
            MyLogger.printd("date: " + cursor.getString(4));
            cursor.moveToNext();
        }
        cursor.close();
        MyLogger.printd("Total num of history: " + cursor.getCount());
    }

    public static ArrayList getAttrHistory(String user, String attr) {

        ArrayList<String> array = new ArrayList<String>();

        Cursor cursor = DbOperations.database.query(DbWrapper.HISTORY,
                new String[] {DbWrapper.HISTORY_VALUE , DbWrapper.HISTORY_DATE },
                DbWrapper.HISTORY_ATTRIBUTE + " = ? AND " + DbWrapper.HISTORY_USERNAME + " = ? ",
                new String[] {attr, user},

                null, null, null);

        cursor.moveToFirst();

        while (!cursor.isAfterLast()) {
            array.add(cursor.getString(0)+getHistoryOldness(cursor.getString(1)));
            cursor.moveToNext();
        }
        cursor.close();

        return array;
    }


    public static String getHistoryOldness(String oldTs) {
MyLogger.printd("Got oldTS: " + oldTs);

        int SECOND = 1000;
        int MINUTE = 60 * SECOND;
        int HOUR = 60 * MINUTE;
        int DAY = 24 * HOUR;

        Long curTs = System.currentTimeMillis();
        String curTsString = curTs.toString();

        long cur = Long.parseLong(curTsString);
        long old = Long.parseLong(oldTs);
        long oldness = cur - old;

        StringBuffer text = new StringBuffer("");
        if (oldness > DAY) {
            text.append(oldness / DAY).append("d ");
            oldness %= DAY;
        }
        text.append(oldness / HOUR).append("h ");
        text.append("ago");

//        Date date=new Date(old);

        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(old);

        int mYear = calendar.get(Calendar.YEAR);
        int mMonth = calendar.get(Calendar.MONTH);
        int mDay = calendar.get(Calendar.DAY_OF_MONTH);

        return "\nDate: "+mDay+"/"+mMonth+" ("+text.toString()+")\n";
//        return text.toString();
    }


    public void deleteHistoryByUser(String username) {

        long retval = DbOperations.database.delete(DbWrapper.HISTORY, " ( "
                +  DbWrapper.HISTORY_USERNAME + " = ? )", new String[] {username});
//        if (retval == 0)
//            MyLogger.printe("History user-deletion failed.");
    }

    public void deleteHistoryByAttr(String name) {
        MyLogger.printd("DELETING history by attribute: " + name);
        long retval = DbOperations.database.delete(DbWrapper.HISTORY, " ( "
                +  DbWrapper.HISTORY_ATTRIBUTE + " = ? )", new String[] {name});
        if (retval == 0)
            MyLogger.printe("History attribute-deletion failed.");
    }

    public void deleteAllHistory() {
        MyLogger.printd("DELETING history records.");

        ArrayList<History> history = getAllHistory();

        for (int i=0; i<history.size(); i++) {
            deleteHistoryByUser(history.get(i).getUsername());
        }
    }

    public ArrayList<History> getAllHistory() {
        ArrayList<History> history = new ArrayList<>();
        Cursor cursor = DbOperations.database.query(DbWrapper.HISTORY,
                new String[] { DbWrapper.HISTORY_USERNAME,  DbWrapper.HISTORY_ATTRIBUTE, DbWrapper.HISTORY_ID, DbWrapper.HISTORY_VALUE, DbWrapper.HISTORY_DATE }, null, null, null, null, null);

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            History h = new History();
            h.setUsername(cursor.getString(0));
            h.setAttribute(cursor.getString(1));
            h.setId(cursor.getInt(2));
            h.setValue(cursor.getString(3));
            h.setDateAsString(cursor.getString(4));
            history.add(h);
            cursor.moveToNext();
        }
        cursor.close();
        return history;
    }


}
