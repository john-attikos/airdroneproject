package di.ptixiaki.droneapp.dbOperations;

import android.content.ContentValues;
import android.database.Cursor;
import android.util.Log;

import java.util.ArrayList;

import di.ptixiaki.droneapp.main.LoginActivity;
import di.ptixiaki.droneapp.dbObjects.User;
import di.ptixiaki.droneapp.debug.MyLogger;

/**
 * Created by sinnaig on 4/5/2015.
 */
public class UserOperations {

    public int checkIfUserExists(String username, String password) {

        String query = "SELECT * FROM "+DbWrapper.USERS+" WHERE "
                + DbWrapper.USERS_USERNAME+" = ? AND "
                + DbWrapper.USERS_PASSWORD+" = ? ";


        Cursor cursor = DbOperations.database.rawQuery(query, new String[]{username, password});
        if ((cursor == null ) || (!cursor.moveToFirst())) {
            return 0;
        } else {
            return 1;
        }
    }

    public int addUser(String username, String password, String email) {

        if (checkIfUserExists(username, password) == 1) {
            //MyLogger.printw("User '" + username + "' already exists");
            return 0;
        }
        ContentValues values = new ContentValues();
        values.put(DbWrapper.USERS_USERNAME, username);
        values.put(DbWrapper.USERS_PASSWORD, password);
        values.put(DbWrapper.USERS_EMAIL, email);
        long retval = DbOperations.database.insert(DbWrapper.USERS, null, values);
        if (retval < 0) {
            MyLogger.printw("USER: '" + username + "'' insertion to database failed");
            return -1;
        }
        return 1;
    }


    /// for debugging
    public void printAllUsers() {
        MyLogger.printd("About to print all users");

        Cursor cursor = DbOperations.database.query(DbWrapper.USERS,
                new String[] {DbWrapper.USERS_USERNAME, DbWrapper.USERS_PASSWORD,DbWrapper.USERS_EMAIL},
                null,  null,  null,  null,  null);
        cursor.moveToFirst();

        while(!cursor.isAfterLast()) {
            MyLogger.printd(" \n ++++++++++++++++++");
            MyLogger.printd("username: "+cursor.getString(0));
            MyLogger.printd("password: " + cursor.getString(1));
            MyLogger.printd("email: " + cursor.getString(2));
            cursor.moveToNext();
        }
        cursor.close();
        MyLogger.printd("Total num of USERS: " + cursor.getCount());

    }


    public void setUserLoggedIn(String username, String password) {

        if (checkIfUserExists(username, password) == 0)
            Log.d("", "+++user: "+username+" does not exists");

        setAllUsersLoggedOut();

        ContentValues values = new ContentValues();
        values.put(DbWrapper.USER_IS_LOGGEDIN, "yes");

        DbOperations.database.update(DbWrapper.USERS,
                values,
                DbWrapper.USERS_USERNAME + " = ? ",
                new String[]{username});

    }


    public void setAllUsersLoggedOut() {
        ContentValues values = new ContentValues();
        values.put(DbWrapper.USER_IS_LOGGEDIN, "no");

        DbOperations.database.update(DbWrapper.USERS,
                values, null, null);
    }

    public User getLoggedInUser() {

        User user = new User();

        Cursor cursor = DbOperations.database.query(DbWrapper.USERS,
                new String[] {DbWrapper.USERS_USERNAME , DbWrapper.USERS_PASSWORD, DbWrapper.USERS_EMAIL, DbWrapper.USER_IS_LOGGEDIN },
                DbWrapper.USER_IS_LOGGEDIN + " = ? ",
                new String[] {"yes"},
                null, null, null);

        cursor.moveToFirst();


        if (!cursor.isAfterLast()) {
            user.setUsername(cursor.getString(0));
            user.setPassword(cursor.getString(1));
            user.setEmail(cursor.getString(2));
            user.setIsLoggedIn(cursor.getString(3));
            cursor.close();
            return user;
        }
        Log.d("", "+++nobody is logged in....");
        return user;

    }


    public void deleteUser(String username) {
        MyLogger.printd("DELETING user: " + username);

        LoginActivity.database.historyOp.deleteHistoryByUser(username);
        LoginActivity.database.userHasAttributesOp.deleteUserHasAttributesByName(username);

        long retval = DbOperations.database.delete(DbWrapper.USERS, " ( "
                +  DbWrapper.USERS_USERNAME + " = ? )", new String[] {username});
        if (retval == 0)
            MyLogger.printe("Client deletion failed.");
    }

    public void deleteAllUsers() {
        MyLogger.printd("DELETING all users.");

        ArrayList<User> users = getAllUsers();

        for (int i=0; i<users.size(); i++) {
            deleteUser(users.get(i).getUsername());
        }
    }

    public ArrayList<User> getAllUsers() {
        ArrayList<User> users = new ArrayList<>();
        Cursor cursor = DbOperations.database.query(DbWrapper.USERS,
                new String[] { DbWrapper.USER_IS_LOGGEDIN,  DbWrapper.USERS_USERNAME, DbWrapper.USERS_PASSWORD, DbWrapper.USERS_EMAIL }, null, null, null, null, null);

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            User user = new User();
            user.setIsLoggedIn(cursor.getString(0));
            user.setUsername(cursor.getString(1));
            user.setPassword(cursor.getString(2));
            users.add(user);
            cursor.moveToNext();
        }
        cursor.close();
        return users;
    }


}
