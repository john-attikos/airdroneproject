package di.ptixiaki.droneapp.dbOperations;

import android.content.ContentValues;
import android.database.Cursor;
import android.util.Log;

import java.util.ArrayList;

import di.ptixiaki.droneapp.debug.MyLogger;
import di.ptixiaki.droneapp.main.LoginActivity;

/**
 * Created by sinnaig on 4/5/2015.
 */
public class UserHasAttributesOperations {

    public UserOperations userOp = new UserOperations();

    public void addRecord(String username, String attr) {


        if (checkIfRecordExists(username, attr) == 1) {
            MyLogger.printd("record (" + username + "," + attr + ") exists!");
            return;
        }

        LoginActivity.database.attributesOp.addAttribute(attr);

        ContentValues values = new ContentValues();
        values.put(DbWrapper.USERS_HAS_ATTRIBUTES_USER, username);
        values.put(DbWrapper.USERS_HAS_ATTRIBUTES_ATTRIBUTE, attr);
        long retval = DbOperations.database.insert(DbWrapper.USERS_HAS_ATTRIBUTES, null, values);
        if (retval < 0)
            Log.e("", "///+++Statistic insertion to database failed!");
    }



    public int checkIfRecordExists(String username, String attr) {

        String query = "SELECT * FROM "+DbWrapper.USERS_HAS_ATTRIBUTES+" WHERE "
                + DbWrapper.USERS_HAS_ATTRIBUTES_USER+" = ? AND "
                + DbWrapper.USERS_HAS_ATTRIBUTES_ATTRIBUTE+" = ? ";


        Cursor cursor = DbOperations.database.rawQuery(query, new String[]{username, attr});
        if ((cursor == null ) || (!cursor.moveToFirst())) {
            return 0;
        } else {
            return 1;
        }
    }


    /// for debugging
    public void printAllUsersHasAttributes() {
        MyLogger.printd("About to print all records of User_has_attributes");

        Cursor cursor = DbOperations.database.query(DbWrapper.USERS_HAS_ATTRIBUTES,
                new String[] {DbWrapper.USERS_HAS_ATTRIBUTES_USER, DbWrapper.USERS_HAS_ATTRIBUTES_ATTRIBUTE},
                null,  null,  null,  null,  null);
        cursor.moveToFirst();

        while(!cursor.isAfterLast()) {
            MyLogger.printd("n ++++++++++++++++++");
            MyLogger.printd("username: "+cursor.getString(0));
            MyLogger.printd("attribute: " + cursor.getString(1));
            cursor.moveToNext();
        }
        cursor.close();
        MyLogger.printd("Total num of USERS_HAS_ATTRIBUTES: "+cursor.getCount());
    }


    public static ArrayList<String> getUserAttributes(String user) {

        ArrayList<String> array = new ArrayList<String>();

        Cursor cursor = DbOperations.database.query(DbWrapper.USERS_HAS_ATTRIBUTES,
                new String[] {DbWrapper.USERS_HAS_ATTRIBUTES_ATTRIBUTE },
                DbWrapper.USERS_HAS_ATTRIBUTES_USER + " = ? ",
                new String[] {user},
                null, null, null);

        cursor.moveToFirst();

        while (!cursor.isAfterLast()) {
            array.add(cursor.getString(0));
            cursor.moveToNext();
        }
        cursor.close();

        return array;
    }


    public void deleteUserHasAttributesByName(String username) {
        long retval = DbOperations.database.delete(DbWrapper.USERS_HAS_ATTRIBUTES, " ( "
                +  DbWrapper.USERS_HAS_ATTRIBUTES_USER + " = ? )", new String[] {username});
        if (retval == 0)
            MyLogger.printw("UserHasAttribute record deletion failed.");
    }



    public void deleteUserHasAttributesByAttr(String name) {
        MyLogger.printd("DELETING from User_has_attribute where attribute = " + name);
        long retval = DbOperations.database.delete(DbWrapper.USERS_HAS_ATTRIBUTES, " ( "
                +  DbWrapper.USERS_HAS_ATTRIBUTES_USER + " = ? )", new String[] {name});
        if (retval == 0)
            MyLogger.printe("UserHasAttribute record deletion failed.");
    }


    public static ArrayList<String> getUserLogos(String user)
    {
        ArrayList<String> attributes = new ArrayList<String>();
        ArrayList<String> userLogos = new ArrayList<String>();

        attributes = getUserAttributes(user);

        String logo_name = new String();
        String logo_name_fixed = new String();
        // the way of converting the attribute to logo name is standard
        // e.g.: "random attribute name" -> "logo_random_attribute_name.png"

        for (int i=0; i<attributes.size(); i++)
        {
            logo_name = "logo_" + attributes.get(i) + ".png";
            logo_name_fixed = logo_name.replaceAll("\\s+", "_");
                                            // 1st ^^ argument is REGEX ("\s" is whitespace)
            userLogos.add(i, logo_name_fixed);
        }

        return userLogos;
    }



}
