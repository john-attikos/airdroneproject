package di.ptixiaki.droneapp.dbOperations;

import android.content.ContentValues;
import android.database.Cursor;

import java.util.ArrayList;

import di.ptixiaki.droneapp.main.LoginActivity;
import di.ptixiaki.droneapp.dbObjects.Attribute;
import di.ptixiaki.droneapp.debug.MyLogger;

/**
 * Created by sinnaig on 4/5/2015.
 */
public class AttributesOperations {

    public int checkIfAttributeExists(String attrName) {

        String query = "SELECT * FROM "+DbWrapper.ATTRIBUTES+" WHERE "
                + DbWrapper.ATTRIBUTES_NAME+" = ? ";


        Cursor cursor = DbOperations.database.rawQuery(query, new String[] {attrName} );
        if ((cursor == null ) || (!cursor.moveToFirst())) {
            return 0;
        } else {
            return 1;
        }
    }


    public int addAttribute(String attrName) {

        if (checkIfAttributeExists(attrName) == 1) {
            return 0;
        }

        ContentValues values = new ContentValues();
        values.put(DbWrapper.ATTRIBUTES_NAME, attrName);
        long retval = DbOperations.database.insert(DbWrapper.ATTRIBUTES, null, values);
        if (retval < 0) {
            MyLogger.printw("ATTRIBUTES insertion to database failed");
            return -1;
        }
        return 0;
    }



    /// for debugging
    public void printAllAttributes() {
        MyLogger.printd("About to print all attributes");

        Cursor cursor = DbOperations.database.query(DbWrapper.ATTRIBUTES,
                new String[] {DbWrapper.ATTRIBUTES_NAME},
                null,  null,  null,  null,  null);
        cursor.moveToFirst();
        cursor.getCount();
        while(!cursor.isAfterLast()) {
            MyLogger.printd("++++++++++++++++++");
            MyLogger.printd("attributes name: "+cursor.getString(0));
            cursor.moveToNext();
        }
        cursor.close();
        MyLogger.printd("Total num of ATTRIBUTES: "+cursor.getCount());

    }


    public void deleteAttr(String attr) {
        MyLogger.printd("DELETING attr: " + attr);

        LoginActivity.database.historyOp.deleteHistoryByAttr(attr);
        LoginActivity.database.userHasAttributesOp.deleteUserHasAttributesByAttr(attr);

        long retval = DbOperations.database.delete(DbWrapper.ATTRIBUTES, " ( "
                + DbWrapper.ATTRIBUTES_NAME + " = ? )", new String[]{attr});
        if (retval == 0)
            MyLogger.printe("Attribute deletion failed.");
    }

    public void deleteAllAttr() {
        MyLogger.printd("DELETING all attributes.");

        ArrayList<Attribute> attr = getAllAttr();

        for (int i=0; i<attr.size(); i++) {
            deleteAttr(attr.get(i).getName());
        }
    }

    public ArrayList<Attribute> getAllAttr() {
        ArrayList<Attribute> attr = new ArrayList<Attribute>();
        Cursor cursor = DbOperations.database.query(DbWrapper.ATTRIBUTES,
                new String[] { DbWrapper.ATTRIBUTES_NAME }, null, null, null, null, null);

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            Attribute a = new Attribute();
            a.setName(cursor.getString(0));
            attr.add(a);
            cursor.moveToNext();
        }
        cursor.close();
        return attr;
    }



}
