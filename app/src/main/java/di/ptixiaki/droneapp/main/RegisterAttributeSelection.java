package di.ptixiaki.droneapp.main;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.SparseBooleanArray;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.concurrent.ExecutionException;

import di.ptixiaki.droneapp.R;
import di.ptixiaki.droneapp.webServices.GetAllAttributes;

public class RegisterAttributeSelection extends Activity implements
        OnClickListener {
    Button button;
    ListView listView;
    ArrayAdapter<String> adapter;

    public static ArrayList<String> allAttributes = new ArrayList<>();

    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.register_attr_selection);

        findViewsById();

        // New User popup
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Hello and welcome to DroneApp!\n\nLet's begin with selecting the attributes of your interest...")
                .setCancelable(true)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        //do things
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();

        try {
            // Beginning communication with server
            // Better wait for the update to complete before we continue
            Boolean network = new GetAllAttributes().execute().get();

            if ( !network )
            {
                Toast.makeText(this, "Please connect to the internet before trying to register!", Toast.LENGTH_LONG).show();
                finish();
            }

        }
        catch (InterruptedException e) {
            e.printStackTrace();
        }
        catch (ExecutionException e) {
            System.out.println("Execution Exception");
            e.printStackTrace();
        }

        String[] attr = new String[allAttributes.size()];
        for (int i = 0; i < allAttributes.size(); i++) {
            attr[i] = allAttributes.get(i);
        }

        adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_multiple_choice, attr);
        listView.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);
        listView.setAdapter(adapter);

        button.setOnClickListener(this);
    }

    private void findViewsById() {
        listView = (ListView) findViewById(R.id.list);
        button = (Button) findViewById(R.id.attr_ok_button);
    }

    public void onClick(View v) {
        SparseBooleanArray checked = listView.getCheckedItemPositions();
        ArrayList<String> selectedItems = new ArrayList<String>();


        for (int i = 0; i < checked.size(); i++) {
            int position = checked.keyAt(i);

            if (checked.valueAt(i))
                selectedItems.add(adapter.getItem(position));

        }

        String[] outputStrArr = new String[selectedItems.size()];

        for (int i = 0; i < selectedItems.size(); i++) {
            outputStrArr[i] = selectedItems.get(i);
        }

        // We won't allow the user to continue without selecting at least one attribute!
        if (selectedItems.size() <= 0)
        {
            Toast.makeText(this, "You must select at least one attribute!",
                    Toast.LENGTH_LONG).show();
        }
        else {

            Intent intent = new Intent(getApplicationContext(),
                    ResultActivity.class);

            // Create a bundle object
            Bundle b = new Bundle();
            b.putStringArray("selectedItems", outputStrArr);

            // Add the bundle to the intent.
            intent.putExtras(b);

            // start the ResultActivity
            startActivity(intent);
        }
    }
}