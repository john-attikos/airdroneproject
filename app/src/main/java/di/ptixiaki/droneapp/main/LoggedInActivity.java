package di.ptixiaki.droneapp.main;

import android.app.ActionBar;
import android.app.ActionBar.Tab;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.concurrent.ExecutionException;

import di.ptixiaki.droneapp.R;
import di.ptixiaki.droneapp.adapter.TabsPagerAdapter;
import di.ptixiaki.droneapp.storage.IntStorage;
import di.ptixiaki.droneapp.webServices.UpdateAttributes;
import di.ptixiaki.droneapp.webServices.UpdateDroneCondition;
import di.ptixiaki.droneapp.webServices.UpdateHistory;
import di.ptixiaki.droneapp.webServices.UpdateUserAttributeLogos;

public class LoggedInActivity extends FragmentActivity implements ActionBar.TabListener {

	private ViewPager viewPager;
	private TabsPagerAdapter mAdapter;
	private ActionBar actionBar;
	public static ArrayList<String> sessionLogos = new ArrayList<>();

	// Tab titles
	private String[] tabs = { "Drone", "Farm", "Photos" };

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_loggedin);

		getActionBar().setTitle("Welcome " + LoginActivity.user);

		try {
			// Beginning communication with server
			// Better wait for the update to complete before we continue, otherwise the FARM tab may have incomplete data
			Boolean network = new UpdateAttributes().execute().get();
			new UpdateUserAttributeLogos().execute().get();
//			new GetDroneImages().execute().get();
			new UpdateDroneCondition().execute().get();
			new UpdateHistory().execute();

			if ( !network )
				Toast.makeText(this, "Network unavailable - showing data from latest update", Toast.LENGTH_LONG).show();
			else
				Toast.makeText(this, "Data successfully retrieved from server!", Toast.LENGTH_LONG).show();

		}
		catch (InterruptedException e) {
			e.printStackTrace();
		}
		catch (ExecutionException e) {
			System.out.println("Execution Exception");
			e.printStackTrace();
		}

        // creating application's dir in SD card if not exists (this dir contains the images)
        IntStorage.createAppDir();
		// each user has a different dir which contains his and only images
		IntStorage.createUserAppDir(LoginActivity.user);

		// Initialization
		viewPager = (ViewPager) findViewById(R.id.pager);
		actionBar = getActionBar();
		mAdapter = new TabsPagerAdapter(getSupportFragmentManager());

		viewPager.setAdapter(mAdapter);
		actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);

		// Adding Tabs
		for (String tab_name : tabs) {
			actionBar.addTab(actionBar.newTab().setText(tab_name).setTabListener(this));
		}

		viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

			@Override
			public void onPageSelected(int position) {
				// on changing the page
				// make respected tab selected
				actionBar.setSelectedNavigationItem(position);
			}

			@Override
			public void onPageScrolled(int arg0, float arg1, int arg2) {
			}

			@Override
			public void onPageScrollStateChanged(int arg0) {
			}
		});
	}

	@Override
	public void onTabReselected(Tab tab, FragmentTransaction ft) {
	}

	@Override
	public void onTabSelected(Tab tab, FragmentTransaction ft) {
		// on tab selected
		// show respected fragment view
		viewPager.setCurrentItem(tab.getPosition());
	}

	@Override
	public void onTabUnselected(Tab tab, FragmentTransaction ft) {
	}


}
