package di.ptixiaki.droneapp.main;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import di.ptixiaki.droneapp.R;
import di.ptixiaki.droneapp.conditionPackage.DummyDrone;


// NOTE: this activity gets dummy info from server since we don't have a drone :)
public class ConditionFragment extends Fragment {

    private ProgressBar progressBar;
    private TextView batteryPercentage;
    private TextView distanceValue;
    private TextView flightTimeValue;
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_condition, container, false);

        //filling progress bar according to battery state
        progressBar = (ProgressBar) rootView.findViewById(R.id.progressBar);
        progressBar.setProgress(DummyDrone.getDroneBatteryLevel());

        //setting progress bar percentage
        batteryPercentage = (TextView) rootView.findViewById(R.id.text_battery_percentage);
        batteryPercentage.setText("("+String.valueOf(DummyDrone.getDroneBatteryLevel())+"%)");

        distanceValue = (TextView) rootView.findViewById(R.id.distance_value);
        distanceValue.setText(String.valueOf(DummyDrone.getDistanceFromBase())+" meter(s)");

        flightTimeValue = (TextView) rootView.findViewById(R.id.flight_time_value);
        flightTimeValue.setText(String.valueOf(DummyDrone.getFlightTime())+"h"+String.valueOf(DummyDrone.getFlightTime())+"m");

        return rootView;
	}
}
