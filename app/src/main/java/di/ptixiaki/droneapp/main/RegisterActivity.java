package di.ptixiaki.droneapp.main;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.concurrent.ExecutionException;

import di.ptixiaki.droneapp.R;
import di.ptixiaki.droneapp.webServices.Register;


public class RegisterActivity extends Activity {

    public static String[] resultArr;
    public static String user  = new String();
    public static String pass  = new String();
    public static String email = new String();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Set View to register.xml
        setContentView(R.layout.register);
        
        TextView loginScreen = (TextView) findViewById(R.id.link_to_login);


        Bundle b = getIntent().getExtras();
        resultArr = b.getStringArray("selectedItems");

        // Listening to Login Screen link
        loginScreen.setOnClickListener(new View.OnClickListener() {
			
			public void onClick(View arg0) {
				// Switching to Login Screen/closing register screen

                Intent intent = new Intent(getApplicationContext(),LoginActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);

			}
		});
    }

    public void register(View view) {
        EditText editTextUser = (EditText) findViewById(R.id.reg_fullname);
        EditText editTextEmail = (EditText) findViewById(R.id.reg_email);
        EditText editTextPass = (EditText) findViewById(R.id.reg_password);
        user  = editTextUser.getText().toString();
        email = editTextEmail.getText().toString();
        pass  = editTextPass.getText().toString();

        if (!Network.haveNetworkConnection(LoginActivity.context))
        {
            Toast.makeText(this, "Please connect to the internet before registering!",
                    Toast.LENGTH_LONG).show();
        }
        else if (!validInput())
        {
            recreate();
        }
        else
        {
            Toast.makeText(this, "Registration complete, you can now login!",
                    Toast.LENGTH_LONG).show();

            Intent intent = new Intent(getApplicationContext(),LoginActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
        }
    }


    boolean validInput()
    {
        if (!isEmailValid(email))
        {
            Toast.makeText(this, "EMAIL does not have valid format!",
                    Toast.LENGTH_LONG).show();
            return false;
        }
        else if (!isPasswordValid(pass))
        {
            Toast.makeText(this, "PASSWORD must be at least 4 characters!",
                    Toast.LENGTH_LONG).show();
            return false;
        }
        else if (!isUsernameValid(user))
        {
            return false;
        }

        return true;
    }

    boolean isUsernameValid(String user)
    {
        boolean validUsername = false;

        if (user.length() < 4)
        {
            Toast.makeText(this, "Username too small! (must be at least 4 characters)",
                    Toast.LENGTH_LONG).show();

            return validUsername;
        }
        try {
            validUsername = new Register().execute().get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
        if (!validUsername) {
            Toast.makeText(this, "USERNAME exists, try a different one!",
                    Toast.LENGTH_LONG).show();
        }

        return validUsername;
    }


    boolean isEmailValid(CharSequence email)
    {
        boolean result = false;

        result = android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();

        return result;
    }


    boolean isPasswordValid(String pass)
    {
        boolean result = true;

        if (pass.length() < 4)
        {
            result = false;
        }

        return result;
    }


}