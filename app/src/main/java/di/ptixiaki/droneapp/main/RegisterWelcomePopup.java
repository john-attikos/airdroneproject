package di.ptixiaki.droneapp.main;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

import di.ptixiaki.droneapp.R;

/**
 * Created by sinnaig on 12/6/2016.
 */

public class RegisterWelcomePopup extends Activity implements OnClickListener {

    Button ok_btn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.register_popup);

        ok_btn = (Button) findViewById(R.id.attr_ok_button);

        ok_btn.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        Intent i = new Intent(getApplicationContext(), RegisterAttributeSelection.class);
        startActivity(i);
    }


}

