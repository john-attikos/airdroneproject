package di.ptixiaki.droneapp.main;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.concurrent.ExecutionException;

import di.ptixiaki.droneapp.R;
import di.ptixiaki.droneapp.dbOperations.DbOperations;
import di.ptixiaki.droneapp.webServices.Login;


public class LoginActivity extends Activity {

    public static String baseUrl;
    public static String loginUrl;
    public static String registerUrl;
    public static String updateAttributesUrl;
    public static String getAllAttributesUrl;
    public static String updateAttributeLogoUrl;
    public static String getDroneImagesUrl;
    public static String updateHistoryUrl;
    public static String updateDroneConditionUrl;
    public static String imageReceiverUrl;

    public static Context context;
    public static DbOperations database;
    public static String user = new String();
    public static String pass = new String();

    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.login);

        this.context = this;
        TextView registerScreen = (TextView) findViewById(R.id.link_to_register);

        setTitle(R.string.login);

        // Firstly, read properties from the file
        PropertyFile props      = new PropertyFile(this);
        baseUrl                 = props.getServerIp() + props.getServerPort();
        loginUrl                = baseUrl + props.getLoginUrl();
        registerUrl             = baseUrl + props.getRegisterUrl();
        updateAttributesUrl     = baseUrl + props.getAttributesUrl();
        getAllAttributesUrl     = baseUrl + props.getAllAttributesUrl();
        updateAttributeLogoUrl  = baseUrl + props.getAttributeLogoUrl();
        getDroneImagesUrl       = baseUrl + props.getDroneImagesUrl();
        updateHistoryUrl        = baseUrl + props.getHistoryUrl();
        imageReceiverUrl        = baseUrl + props.getImageReceiverUrl();
        updateDroneConditionUrl = baseUrl + props.getDroneConditionUrl();

        //opening database
        database = new DbOperations(this);
        database.openDB();

        // Listening to register new account link
        registerScreen.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                // Switching to Register screen
                Intent i = new Intent(getApplicationContext(), RegisterAttributeSelection.class);

                startActivity(i);
            }
        });

    }

    @Override
    protected void onDestroy() {
        database.closeDB();
        super.onDestroy();
    }

    public void login(View view) {
        Intent intent = new Intent(this, LoggedInActivity.class);
        EditText editTextUser = (EditText) findViewById(R.id.input_username);
        EditText editTextPass = (EditText) findViewById(R.id.input_password);
        user = editTextUser.getText().toString();
        pass = editTextPass.getText().toString();

        ////////////////////////////////////////////////////////////////////////
        // For development purposes, let's connect as admin when empty fields //
//        if (user.equalsIgnoreCase(""))
//            user = "admin";
//        if (pass.equalsIgnoreCase(""))
//            pass = "admin";
        ///////////////////////////////////////////////////////

        Boolean userExistence;
        try {

            // Will try to login using the server
            // If network unavailable, will try local login
            userExistence = new Login().execute().get();

            if ( !userExistence ) {
                Toast.makeText(this, "Wrong Username or Password", Toast.LENGTH_LONG).show();
                editTextUser.setText("");
                editTextPass.setText("");
                return;
            }
        }
        catch (InterruptedException e) {
            e.printStackTrace();
        }
        catch (ExecutionException e) {
            System.out.println("Execution Exception");
            e.printStackTrace();
        }

        // Once we are connected, next activity is "LoggedInActivity"
        intent.putExtra("User: ", user);
        startActivity(intent);
    }




}