package di.ptixiaki.droneapp.main;

import android.app.Activity;
import android.content.Context;
import android.graphics.Point;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupWindow;

import java.util.ArrayList;

import di.ptixiaki.droneapp.R;
import di.ptixiaki.droneapp.adapter.FarmCustomAdapter;
import di.ptixiaki.droneapp.dbOperations.UserHasAttributesOperations;

public class FarmFragment extends Fragment {

    ListView lv;
    Context context;
    ArrayList list;
    Point p;

    //first time in app info will be stored in db
    boolean firstTimeInApp = true;

    ArrayList<String> array = UserHasAttributesOperations.getUserAttributes(LoginActivity.user);
    public final String[] prgmNameList = array.toArray(new String[array.size()]);


    public ArrayList<String> userLogos = UserHasAttributesOperations.getUserLogos(LoginActivity.user);
    public String[] prgmImages = new String[userLogos.size()];


	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

        prgmImages = userLogos.toArray(prgmImages);

		View rootView = inflater.inflate(R.layout.fragment_farm, container, false);
        lv = (ListView) rootView.findViewById(R.id.listView);
        lv.setAdapter(new FarmCustomAdapter(this, prgmNameList, prgmImages));

		return rootView;
	}


    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser) {
            if (firstTimeInApp) {

/* enable popup in the future (if necessary) */
//                showPopup(this.getActivity());
                //store in db not first time user...
                //db.setFirstTime("username", false);
                firstTimeInApp = false;
            }
        }
    }

    // First-time user popup (not activated but fully functional)
    private void showPopup(final Activity context) {
        int popupWidth = 300;
        int popupHeight = 250;

        int[] location = new int[2];
        location[0] = 0;
        location[1] = 0;

        p = new Point();
        p.x = location[0];
        p.y = location[1];

        // Inflate the popup_layout.xml
        LinearLayout viewGroup = (LinearLayout) context.findViewById(R.id.popup);
        LayoutInflater layoutInflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View layout = layoutInflater.inflate(R.layout.popup_layout, viewGroup);

        // Creating the PopupWindow
        final PopupWindow popup = new PopupWindow(context);
        popup.setContentView(layout);
        popup.setWidth(popupWidth);
        popup.setHeight(popupHeight);
        popup.setFocusable(true);

        // Some offset to align the popup a bit to the right, and a bit down, relative to button's position.
        int OFFSET_X = 30;
        int OFFSET_Y = 100;

        // Clear the default translucent background
        popup.setBackgroundDrawable(new BitmapDrawable());

        // Displaying the popup at the specified location, + offsets.
        popup.showAtLocation(layout, Gravity.CENTER, p.x + OFFSET_X, p.y + OFFSET_Y);
    }


}

