package di.ptixiaki.droneapp.main;

import android.content.Context;
import android.content.res.AssetManager;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import di.ptixiaki.droneapp.debug.MyLogger;

/**
 * Created by sinnaig on 2/4/2016.
 */

public class PropertyFile {
    private Context context;
    private Properties properties;


    public PropertyFile(Context context) {
        this.context = context;
        properties = new Properties();
    }

    private Properties getProperties(String FileName) {
        try {
            //access to the folder ‘assets’
            AssetManager am = context.getAssets();

            //opening the file
            InputStream inputStream = am.open(FileName);

            //loading of the properties
            properties.load(inputStream);

        } catch (IOException e) {
            MyLogger.printe("PropertyFile class: " + e.toString());
        }

        return properties;
    }

    public String getServerIp()
    {
        Properties prop = getProperties("config.properties");

        return prop.getProperty("serverIP");
    }

    public String getServerPort()
    {
        Properties prop = getProperties("config.properties");

        return prop.getProperty("serverPort");
    }

    public String getLoginUrl()
    {
        Properties prop = getProperties("config.properties");

        return prop.getProperty("loginUrl");
    }

    public String getRegisterUrl()
    {
        Properties prop = getProperties("config.properties");

        return prop.getProperty("registerUrl");
    }

    public String getAttributesUrl()
    {
        Properties prop = getProperties("config.properties");

        return prop.getProperty("updateAttributesUrl");
    }

    public String getAllAttributesUrl()
    {
        Properties prop = getProperties("config.properties");

        return prop.getProperty("getAllAttributesUrl");
    }

    public String getAttributeLogoUrl()
    {
        Properties prop = getProperties("config.properties");

        return prop.getProperty("updateAttributeLogoUrl");
    }

    public String getDroneImagesUrl()
    {
        Properties prop = getProperties("config.properties");

        return prop.getProperty("getDroneImagesUrl");
    }

    public String getHistoryUrl()
    {
        Properties prop = getProperties("config.properties");

        return prop.getProperty("updateHistoryUrl");
    }

    public String getImageReceiverUrl()
    {
        Properties prop = getProperties("config.properties");

        return prop.getProperty("imageReceiverUrl");
    }

    public String getDroneConditionUrl()
    {
        Properties prop = getProperties("config.properties");

        return prop.getProperty("updateDroneConditionUrl");
    }



}
