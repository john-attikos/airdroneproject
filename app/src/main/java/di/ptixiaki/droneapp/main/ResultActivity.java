package di.ptixiaki.droneapp.main;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;

import di.ptixiaki.droneapp.R;

public class ResultActivity extends Activity implements OnClickListener{

    Button next;
    ListView lv;
    String[] resultArr;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.register_selected_attributes);

        findViewsById();

        Bundle b = getIntent().getExtras();
        resultArr = b.getStringArray("selectedItems");

        lv.setClickable(true);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1, resultArr);
        lv.setAdapter(adapter);

        next.setOnClickListener(this);

    }

    private void findViewsById() {
        lv = (ListView) findViewById(R.id.outputlist);
        next = (Button) findViewById(R.id.attr_next_button);
    }


    public void onClick(View v) {

        Intent intent = new Intent(getApplicationContext(),
                RegisterActivity.class);

        // Create a bundle object
        Bundle b = new Bundle();
        b.putStringArray("selectedItems", resultArr);

        // Add the bundle to the intent.
        intent.putExtras(b);

        // start the ResultActivity
        startActivity(intent);
    }


}