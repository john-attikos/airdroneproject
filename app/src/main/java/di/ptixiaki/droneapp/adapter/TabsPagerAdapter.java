package di.ptixiaki.droneapp.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import di.ptixiaki.droneapp.imagePresentation.ImageFragment;
import di.ptixiaki.droneapp.main.ConditionFragment;
import di.ptixiaki.droneapp.main.FarmFragment;

public class TabsPagerAdapter extends FragmentPagerAdapter {

	public TabsPagerAdapter(FragmentManager fm) {
		super(fm);
	}

	@Override
	public Fragment getItem(int index) {

		switch (index) {
		case 0:
			// Drone condition fragment activity
			return new ConditionFragment();
		case 1:
			// Farm fragment activity
			return new FarmFragment();
		case 2:
			// Image fragment activity
			return new ImageFragment();
		}

		return null;
	}

	@Override
	public int getCount() {
		// get item count - equal to number of tabs
		return 3;
	}

}
