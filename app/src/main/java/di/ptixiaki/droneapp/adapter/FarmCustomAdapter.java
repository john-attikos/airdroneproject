package di.ptixiaki.droneapp.adapter;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import di.ptixiaki.droneapp.R;
import di.ptixiaki.droneapp.dbOperations.HistoryOperations;
import di.ptixiaki.droneapp.main.FarmFragment;
import di.ptixiaki.droneapp.main.LoginActivity;
import di.ptixiaki.droneapp.storage.IntStorage;

/**
 * Created by root on 17/4/2015.
 */

public class FarmCustomAdapter extends BaseAdapter{
    String [] result;
    Context context;
    String [] imageId;

    String[] attributesNames;
    String[] attributesImages;

    private static LayoutInflater inflater=null;
        public FarmCustomAdapter(FarmFragment mainActivity, String[] prgmNameList, String[] prgmImages) {
        // TODO Auto-generated constructor stub
        result=prgmNameList;
        attributesNames = prgmNameList;
        attributesImages = prgmImages;
        context=mainActivity.getActivity();
        imageId=prgmImages;
        inflater = ( LayoutInflater )context.
                getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }
    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return result.length;
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    public class Holder
    {
        TextView tv;
        ImageView img;
    }
    @Override
    public View getView(final int position, final View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        Holder holder=new Holder();
        View rowView;
        rowView = inflater.inflate(R.layout.farm_list, null);
        holder.tv=(TextView) rowView.findViewById(R.id.textView1);
        holder.img=(ImageView) rowView.findViewById(R.id.imageView1);
        holder.tv.setText(result[position]);

        ArrayList<Drawable> d_arraryList = new ArrayList<Drawable>();
        for (int i=0; i<imageId.length; i++)
        {
            d_arraryList.add(i, getDrawableFromPath(IntStorage.getDirPathForLogos()+ "/" + imageId[i]));
        }
        final Drawable[] d_array = d_arraryList.toArray(new Drawable[d_arraryList.size()]);

        holder.img.setImageDrawable(d_array[position]);


        rowView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ArrayList<String> array = HistoryOperations.getAttrHistory(LoginActivity.user, attributesNames[position]);
                // If user does not have any history for this item
                if (array.size() == 0)
                {
                    array.add(0, "You don't have any history record for " + attributesNames[position]);
                }

                final CharSequence[] items = array.toArray(new CharSequence[array.size()]);


                AlertDialog.Builder builder = new AlertDialog.Builder(
                        new ContextThemeWrapper(context, android.R.style.Theme_Black)
                );
                builder.setTitle(attributesNames[position]);

                builder.setIcon(d_array[position]);

                builder.setItems(items, null);

                builder.setNegativeButton("ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface d, int arg1) {
                        d.cancel();
                    }
                });

                builder.create();
                builder.show();

            }
        });
        return rowView;
    }


    public Drawable getDrawableFromPath(String filePath) {
        Bitmap bitmap = BitmapFactory.decodeFile(filePath);
        return new BitmapDrawable(bitmap);
    }


}