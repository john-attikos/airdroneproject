package di.ptixiaki.droneapp.webServices;

import android.os.AsyncTask;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import di.ptixiaki.droneapp.main.LoginActivity;
import di.ptixiaki.droneapp.main.Network;
import di.ptixiaki.droneapp.conditionPackage.DummyDrone;
import di.ptixiaki.droneapp.dbObjects.User;
import di.ptixiaki.droneapp.debug.MyLogger;


////////////////////////////////////////////////////////////////////////////////////////////////////////////
// NOTE: This service works fine, but gets from server dummy values for drone since we don't have a drone //                                               //
////////////////////////////////////////////////////////////////////////////////////////////////////////////

public class UpdateDroneCondition extends AsyncTask<Void, Void, Boolean> {

    @Override
    public Boolean doInBackground(Void... params) {
        try {

            if ( !Network.haveNetworkConnection(LoginActivity.context) ) {
                DummyDrone.setBatteryState(50);
                DummyDrone.setDistanceFromBase(0);
                DummyDrone.setFlightTime(0);

                return false;
            }
            MyLogger.printd("Getting user's Drone status...");
            HttpHeaders header = new HttpHeaders();
            header.set("user", LoginActivity.user);


            HttpEntity<User> entity = new HttpEntity(header);
            RestTemplate restTemplate = new RestTemplate();
            restTemplate.getMessageConverters().add(new StringHttpMessageConverter());

            HttpEntity<String> response;

            response = restTemplate.exchange(LoginActivity.updateDroneConditionUrl, HttpMethod.GET, entity, String.class);
            String res = response.getBody();

//            MyLogger.printd("Raw response from UpdateDroneCondition: "+res);

            try {
                JSONArray ja = new JSONArray(res);
                JSONObject jo = (JSONObject)ja.get(0);

                DummyDrone.setBatteryState(jo.getInt("batteryState"));
                DummyDrone.setDistanceFromBase(jo.getInt("distanceFromBase"));
                DummyDrone.setFlightTime(jo.getInt("flightTime"));

            } catch (Throwable t) {
                MyLogger.printe("Could not parse received JSON object: (res)\n" + t.getMessage());
            }

        } catch (Exception e) {
            MyLogger.printe("Something went wrong with UpdateHistory \n " + e.getMessage() + e);
        }

        return true;
    }

}