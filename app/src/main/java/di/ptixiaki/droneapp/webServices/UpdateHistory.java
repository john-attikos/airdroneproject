package di.ptixiaki.droneapp.webServices;

import android.os.AsyncTask;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import di.ptixiaki.droneapp.main.LoginActivity;
import di.ptixiaki.droneapp.main.Network;
import di.ptixiaki.droneapp.dbObjects.User;
import di.ptixiaki.droneapp.debug.MyLogger;


public class UpdateHistory extends AsyncTask<Void, Void, Boolean> {

    @Override
    public Boolean doInBackground(Void... params) {

        //////////////////////////////////////////////////////////////////////////
        // If network is unavailable, we'll just get all the data from local DB //
        //////////////////////////////////////////////////////////////////////////

        if ( !Network.haveNetworkConnection(LoginActivity.context) ) {
            return false;
        }

        try {
            MyLogger.printd("Getting user's history...");
            HttpHeaders header = new HttpHeaders();
            header.set("user", LoginActivity.user);

            HttpEntity<User> entity = new HttpEntity(header);
            RestTemplate restTemplate = new RestTemplate();
            restTemplate.getMessageConverters().add(new StringHttpMessageConverter());

            HttpEntity<String> response;

            response = restTemplate.exchange(LoginActivity.updateHistoryUrl, HttpMethod.GET, entity, String.class);
            String res = response.getBody();

            // MyLogger.printd("Raw response from UpdateHistory: "+res);

            // Once the exchange is complete, we'll rewrite the user's history with the latest
            LoginActivity.database.historyOp.deleteHistoryByUser(LoginActivity.user);

            try {
                JSONArray ja = new JSONArray(res);

                for (int i=0; i<ja.length(); i++)
                {
                    JSONObject jo = (JSONObject)ja.get(i);
                    LoginActivity.database.historyOp.addHistory(jo.getString("username"),jo.getString("attribute"),jo.getString("value"),jo.getString("date"));
                }

            } catch (Throwable t) {
                MyLogger.printe("Could not parse received JSON object: (res)\n" + t.getMessage());
            }

        } catch (Exception e) {
            MyLogger.printe("Something went wrong with UpdateHistory \n " + e.getMessage() + e);
        }

        return true;
    }

}