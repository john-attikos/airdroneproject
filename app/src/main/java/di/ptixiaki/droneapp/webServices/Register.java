package di.ptixiaki.droneapp.webServices;

import android.os.AsyncTask;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import di.ptixiaki.droneapp.dbObjects.User;
import di.ptixiaki.droneapp.debug.MyLogger;
import di.ptixiaki.droneapp.main.LoginActivity;
import di.ptixiaki.droneapp.main.Network;
import di.ptixiaki.droneapp.main.RegisterActivity;

/**
 * Created by sinnaig on 11/6/2016.
 */
public class Register extends AsyncTask<Void, Void, Boolean> {

    @Override
    public Boolean doInBackground(Void... params) {

        if ( !Network.haveNetworkConnection(LoginActivity.context) ) {
            MyLogger.printd("Network is not available!");
        }

        try {
            HttpHeaders header = new HttpHeaders();
            header.set("user", RegisterActivity.user);
            header.set("pass", RegisterActivity.pass);
            header.set("email", RegisterActivity.email);

            // Process to pass the new user's attributes
            StringBuilder UserAttributesAsString = new StringBuilder();

            UserAttributesAsString.append(RegisterActivity.resultArr[0]);
            for (int i=1; i<RegisterActivity.resultArr.length; i++)
            {
                UserAttributesAsString.append(" , " + RegisterActivity.resultArr[i]);
            }

            header.set("userAttributes", UserAttributesAsString.toString());

            HttpEntity<User> entity = new HttpEntity(header);
            RestTemplate restTemplate = new RestTemplate();
            restTemplate.getMessageConverters().add(new StringHttpMessageConverter());

            HttpEntity<String> response;

            response = restTemplate.exchange(LoginActivity.registerUrl, HttpMethod.GET, entity, String.class);
            String res = response.getBody();

            MyLogger.printd("Raw res from Register(): " + res);


            if ( res.equals("true")) {
                return true;
            }

        } catch (Exception e) {
            MyLogger.printe("Something went wrong with Register method \n " + e.getMessage() + e);
        }

        return false;
    }

}