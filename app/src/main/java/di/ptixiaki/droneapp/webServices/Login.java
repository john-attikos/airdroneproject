package di.ptixiaki.droneapp.webServices;

import android.os.AsyncTask;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import di.ptixiaki.droneapp.main.LoginActivity;
import di.ptixiaki.droneapp.main.Network;
import di.ptixiaki.droneapp.dbObjects.User;
import di.ptixiaki.droneapp.debug.MyLogger;

/**
 * Created by sinnaig on 16/4/2016.
 */


public class Login extends AsyncTask<Void, Void, Boolean> {

    @Override
    public Boolean doInBackground(Void... params) {

        if ( !Network.haveNetworkConnection(LoginActivity.context) ) {
            MyLogger.printd("Network is not available!");

            if (LoginActivity.database.userOp.checkIfUserExists(LoginActivity.user, LoginActivity.pass) == 1 )
                return true;
            else
                return false;
        }

        try {
            MyLogger.printd("Logging in..." );

            HttpHeaders header = new HttpHeaders();
            header.set("user", LoginActivity.user);
            header.set("pass", LoginActivity.pass);

            HttpEntity<User> entity = new HttpEntity(header);
            RestTemplate restTemplate = new RestTemplate();
            restTemplate.getMessageConverters().add(new StringHttpMessageConverter());

            HttpEntity<String> response;

            response = restTemplate.exchange(LoginActivity.loginUrl, HttpMethod.GET, entity, String.class);
            String res = response.getBody();

            MyLogger.printd("Response from Login(): " + res);


            if ( res.equals("true")) {
                return true;
            }

        } catch (Exception e) {
            MyLogger.printe("Something went wrong with Login method \n " + e.getMessage() + e);
        }

        return false;
    }

}