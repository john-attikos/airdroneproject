package di.ptixiaki.droneapp.webServices;

/**
 * Created by sinnaig on 16/4/2016.
 */
import android.os.AsyncTask;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import di.ptixiaki.droneapp.main.LoginActivity;
import di.ptixiaki.droneapp.main.Network;
import di.ptixiaki.droneapp.dbObjects.User;
import di.ptixiaki.droneapp.debug.MyLogger;


public class UpdateAttributes extends AsyncTask<Void, Void, Boolean> {

    @Override
    public Boolean doInBackground(Void... params) {

        //////////////////////////////////////////////////////////////////////////
        // If network is unavailable, we'll just get all the data from local DB //
        //////////////////////////////////////////////////////////////////////////
        if ( !Network.haveNetworkConnection(LoginActivity.context) ) {
            return false;
        }

        try {
            MyLogger.printd("Getting user's attributes...");
            HttpHeaders header = new HttpHeaders();
            header.set("user", LoginActivity.user);

            HttpEntity<User> entity = new HttpEntity(header);
            RestTemplate restTemplate = new RestTemplate();
            restTemplate.getMessageConverters().add(new StringHttpMessageConverter());

            HttpEntity<String> response;

            response = restTemplate.exchange(LoginActivity.updateAttributesUrl, HttpMethod.GET, entity, String.class);
            String res = response.getBody();

//            MyLogger.printd("Raw response from UpdateAttributes: "+res);

            // Once the exchange is complete, we'll rewrite the user's attributes with the latest
            LoginActivity.database.userHasAttributesOp.deleteUserHasAttributesByName(LoginActivity.user);

            try {
                JSONArray ja = new JSONArray(res);

                for (int i=0; i<ja.length(); i++)
                {
                    JSONObject jo = (JSONObject)ja.get(i);
                    LoginActivity.database.userHasAttributesOp.addRecord(LoginActivity.user, jo.getString("attribute"));
                }

            } catch (Throwable t) {
                MyLogger.printe("Could not parse received JSON object: (res)\n" + t.getMessage());
            }

        } catch (Exception e) {
            MyLogger.printe("Something went wrong with UpdateHistory \n " + e.getMessage() + e);
        }

        return true;
    }

}