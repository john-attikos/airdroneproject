package di.ptixiaki.droneapp.webServices;

import android.os.AsyncTask;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import java.io.File;

import di.ptixiaki.droneapp.debug.MyLogger;
import di.ptixiaki.droneapp.dbObjects.Image;
import di.ptixiaki.droneapp.main.LoginActivity;
import di.ptixiaki.droneapp.storage.IntStorage;

/**
 * Created by sinnaig on 28/4/2016.
 */
public class SendImage extends AsyncTask<Image, Void, Integer> {


    @Override
    public Integer doInBackground(Image... params) {
        try {
            Image image = params[0];

            File file = new File(IntStorage.appImagesPath + image.getFileName());
            if(!file.exists()) {
                return -1;
            }

            HttpHeaders header = new HttpHeaders();
            header.set("user",LoginActivity.user);

            HttpEntity<Image> entity = new HttpEntity(image,header);
            RestTemplate restTemplate = new RestTemplate();
            restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());


            restTemplate.exchange(LoginActivity.imageReceiverUrl, HttpMethod.POST, entity, Image.class);

        } catch (Exception e) {
            MyLogger.printe("Something went wrong with SendImage service: " + e.getMessage() + e);
            return -1;
        }

        return 1;
    }
}


