package di.ptixiaki.droneapp.webServices;

/**
 * Created by sinnaig on 16/4/2016.
 */
import android.os.AsyncTask;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import di.ptixiaki.droneapp.main.LoginActivity;
import di.ptixiaki.droneapp.main.Network;
import di.ptixiaki.droneapp.dbObjects.User;
import di.ptixiaki.droneapp.debug.MyLogger;
import di.ptixiaki.droneapp.main.RegisterAttributeSelection;


public class GetAllAttributes extends AsyncTask<Void, Void, Boolean> {

    @Override
    public Boolean doInBackground(Void... params) {

        ////////////////////////////////////////////////////////////////////////////////////////////////
        // If network is unavailable the registration will fail (this is called only on registration) //
        ////////////////////////////////////////////////////////////////////////////////////////////////
        if ( !Network.haveNetworkConnection(LoginActivity.context) ) {
            return false;
        }

        try {

            HttpHeaders header = new HttpHeaders();
            header.set("user", "NEW_USER");

            HttpEntity<User> entity = new HttpEntity(header);
            RestTemplate restTemplate = new RestTemplate();
            restTemplate.getMessageConverters().add(new StringHttpMessageConverter());

            HttpEntity<String> response;

            response = restTemplate.exchange(LoginActivity.getAllAttributesUrl, HttpMethod.GET, entity, String.class);
            String res = response.getBody();

            try {
                JSONArray ja = new JSONArray(res);

                // If we do not clear the list, it would be keep filling
                // with the same attributes on every back and forth!
                RegisterAttributeSelection.allAttributes.clear();

                for (int i=0; i<ja.length(); i++)
                {
                    JSONObject jo = (JSONObject)ja.get(i);
                    RegisterAttributeSelection.allAttributes.add(i, jo.getString("name"));

//                    LoginActivity.database.userHasAttributesOp.addRecord(LoginActivity.user, jo.getString("attribute"));
                }

            } catch (Throwable t) {
                MyLogger.printe("Could not parse received JSON object: (res)\n" + t.getMessage());
            }

        } catch (Exception e) {
            MyLogger.printe("Something went wrong with GetAllAttributes \n " + e.getMessage() + e);
        }

        return true;
    }

}