package di.ptixiaki.droneapp.webServices;

import android.graphics.Bitmap;
import android.os.AsyncTask;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import di.ptixiaki.droneapp.dbObjects.Image;
import di.ptixiaki.droneapp.dbObjects.User;
import di.ptixiaki.droneapp.debug.MyLogger;
import di.ptixiaki.droneapp.main.LoginActivity;
import di.ptixiaki.droneapp.main.Network;

/**
 * Created by sinnaig on 12/9/2016.
 */



public class GetDroneImages extends AsyncTask<Void, Void, Boolean> {

    @Override
    public Boolean doInBackground(Void... params) {

        //////////////////////////////////////////////////////////////////////////
        // If network is unavailable, we'll just get all the data from local DB //
        //////////////////////////////////////////////////////////////////////////
        if ( !Network.haveNetworkConnection(LoginActivity.context) ) {
            return false;
        }

        try {
            MyLogger.printd("Getting user's Drone Images...");

            HttpHeaders header = new HttpHeaders();
            header.set("user", LoginActivity.user);

            HttpEntity<User> entity = new HttpEntity(header);
            RestTemplate restTemplate = new RestTemplate();
            restTemplate.getMessageConverters().add(new StringHttpMessageConverter());

            HttpEntity<String> response;

            response = restTemplate.exchange(LoginActivity.getDroneImagesUrl, HttpMethod.GET, entity, String.class);
            String res = response.getBody();

//            MyLogger.printd("Raw response from UpdateAttributeLogo: "+res);

            // Once the exchange is complete, we'll rewrite the user's attributes with the latest
//            LoginActivity.database.userHasAttributesOp.deleteUserHasAttributesByName(LoginActivity.user);

            try {
                JSONArray ja = new JSONArray(res);

                for (int i=0; i<ja.length(); i++)
                {
                    JSONObject jo = (JSONObject)ja.get(i);
//                 JSONObject jo = (JSONObject)ja.get(0);
                    Image image = new Image();
                    image.setFileName(jo.getString("fileName"));
                    image.setEncoded(jo.getString("encoded"));

//                    MyLogger.printi("*************");
//                    MyLogger.printi(image.getFileName());
//                    MyLogger.printi(image.getEncoded());
//                    MyLogger.printi(image.getEncoded().length() + " <-- length...");
//                    LoggedInActivity.sessionLogos.add(LoggedInActivity.sessionLogos.size(), image.getFileName());

//                    LoginActivity.database.userHasAttributesOp.addRecord(LoginActivity.user, jo.getString("attribute"));

                    Bitmap bitmap = image.stringToBitmap(image.getEncoded());
                    image.storeDroneImage(bitmap, image.getFileName());

                }

                MyLogger.printd("Image transfer is complete, " + ja.length() + " image(s) are transferred!");


//                image.staticEncoded = image.getEncoded();
//                image.imageToFile(image.getEncoded());

            } catch (Throwable t) {
                MyLogger.printe("Could not parse received JSON object: (res) in GetDroneImages()");
                t.printStackTrace();
            }

        } catch (Exception e) {
            MyLogger.printe("Something went wrong with GetDroneImages()");
            e.printStackTrace();
        }

        return true;
    }

}