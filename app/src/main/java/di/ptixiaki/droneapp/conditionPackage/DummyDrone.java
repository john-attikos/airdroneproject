package di.ptixiaki.droneapp.conditionPackage;

/**
 * Created by root on 19/3/2015.
 */
public class DummyDrone {
    private static int batteryState     = 0;
    private static int distanceFromBase = 0;
    private static int flightTime       = 0;


    public static int getDroneBatteryLevel() {
        return batteryState;
    }

    public static int getDistanceFromBase() {
        return distanceFromBase;
    }

    public static int getFlightTime() {
        return flightTime;
    }

    public static void setBatteryState(int batteryState) {
        DummyDrone.batteryState = batteryState;
    }

    public static void setDistanceFromBase(int distanceFromBase) { DummyDrone.distanceFromBase = distanceFromBase; }

    public static void setFlightTime(int flightTime) {
        DummyDrone.flightTime = flightTime;
    }


}
