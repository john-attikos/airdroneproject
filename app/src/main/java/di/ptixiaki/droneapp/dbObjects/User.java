package di.ptixiaki.droneapp.dbObjects;

/**
 * Created by sinnaig on 4/5/2015.
 */
public class User {

    private String isLoggedIn = new String();
    private String username = new String();
    private String password = new String();
    private String email = new String();


    public User() {
        this.isLoggedIn = "no";
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getIsLoggedIn() {
        return isLoggedIn;
    }

    public void setIsLoggedIn(String isLoggedIn) {
        this.isLoggedIn = isLoggedIn;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
