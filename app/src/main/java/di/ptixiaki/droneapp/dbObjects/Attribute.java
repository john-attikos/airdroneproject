package di.ptixiaki.droneapp.dbObjects;

/**
 * Created by sinnaig on 4/5/2015.
 */
public class Attribute {

    public static String TEMPERATURE 	= "temperature";
    public static String HUMIDITY 	 	= "humidity";
    public static String WIND_SPEED 	= "wind speed";
    public static String WIND_DIRECTION = "wind direction";
    public static String WATERING 		= "watering";
    public static String AIR_PRESSURE 	= "air pressure";
    public static String CLOUDINESS 	= "cloudiness";

    private String name = new String();

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
