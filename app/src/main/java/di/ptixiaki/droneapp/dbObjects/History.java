package di.ptixiaki.droneapp.dbObjects;

/**
 * Created by sinnaig on 4/5/2015.
 */
public class History {

    private int id;
    private String username = new String();
    private String attribute = new String();
    private String value = new String();
    private String dateAsString = new String();


    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getAttribute() {
        return attribute;
    }

    public void setAttribute(String attribute) {
        this.attribute = attribute;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.attribute = username;
    }

    public String getDateAsString() {
        return dateAsString;
    }

    public void setDateAsString(String dateAsString) {
        this.dateAsString = dateAsString;
    }

}
