package di.ptixiaki.droneapp.dbObjects;

import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.BitmapFactory;
import android.media.ExifInterface;
import android.util.Base64;
import android.util.Log;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;

import di.ptixiaki.droneapp.debug.MyLogger;
import di.ptixiaki.droneapp.main.LoginActivity;
import di.ptixiaki.droneapp.storage.IntStorage;

/**
 * Created by sinnaig on 30/4/2016.
 */
public class Image {

    private String encoded = new String();
    private String fileName = new String();

    public static String staticEncoded = new String();

    public Image() {

    }

    public Image(String path, String name) {
        setFileName(name);
        encodeImage(path + name);
    }

    public void encodeImage(String path) {

        File file = new File(path);
        if (!file.exists()) {
            MyLogger.printw("Trying to encode file which does not exist... aborting");
            return;
        }

        Bitmap bm = BitmapFactory.decodeFile(path);
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bm.compress(CompressFormat.JPEG, 100, baos); //bm is the bitmap object
        byte[] b = baos.toByteArray();

        String encodedImage = Base64.encodeToString(b, Base64.DEFAULT);

        setEncoded(encodedImage);
    }

    public String getEncoded() {
        return encoded;
    }

    public void setEncoded(String encoded) {
        this.encoded = encoded;
    }


    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }



    public Bitmap stringToBitmap(String encoded)
    {
        byte[] decodedString = Base64.decode(encoded, Base64.URL_SAFE);
        Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);

        return decodedByte;
    }



    public void storeImage(Bitmap image, String filename) {
        File pictureFile = getOutputMediaFile(filename);
        if (pictureFile == null) {
            Log.d("+++[ERROR]",
                    "Error creating media file, check storage permissions: ");// e.getMessage());
            return;
        }
        try {
            FileOutputStream fos = new FileOutputStream(pictureFile);
            image.compress(Bitmap.CompressFormat.PNG, 90, fos);
            fos.close();
        } catch (FileNotFoundException e) {
            Log.d("[ERROR]", "File not found: " + e.getMessage());
        } catch (IOException e) {
            Log.d("[ERROR]", "Error accessing file: " + e.getMessage());
        }
    }



    public void storeDroneImage(Bitmap image, String filename) {
        File pictureFile = getOutputMediaFileForDroneImages(filename);
        if (pictureFile == null) {
            MyLogger.printe("Error creating media file, check storage permissions...");

            return;
        }
        try {
            FileOutputStream fos = new FileOutputStream(pictureFile);
            image.compress(Bitmap.CompressFormat.PNG, 90, fos);
            fos.close();
        } catch (FileNotFoundException e) {
            MyLogger.printe("File not found: " + e.getMessage());
        } catch (IOException e) {
            MyLogger.printe("Error accessing file: " + e.getMessage());
        }
    }





    private  File getOutputMediaFile(String filename){

        File mediaStorageDir = new File(IntStorage.getDirPathForLogos());

        // Create the storage directory if it does not exist
        if (! mediaStorageDir.exists()){
            if (! mediaStorageDir.mkdirs()){
                return null;
            }
        }
        File mediaFile;
        mediaFile = new File(mediaStorageDir.getPath() + File.separator + filename);
        return mediaFile;
    }



    private  File getOutputMediaFileForDroneImages(String filename){

        File mediaStorageDir = new File(IntStorage.getDirPathForImages() + "/" + LoginActivity.user);

        // Create the storage directory if it does not exist
        if (! mediaStorageDir.exists()){
            if (! mediaStorageDir.mkdirs()){
                return null;
            }
        }
        File mediaFile;
        mediaFile = new File(mediaStorageDir.getPath() + File.separator + filename);
        return mediaFile;
    }


    public static ArrayList<String> getImageLatLon(String path)
    {
        ArrayList<String> latlon = new ArrayList<>();

        ExifInterface exif = null;
        try {
            exif = new ExifInterface(path);
        } catch (IOException e) {
            e.printStackTrace();
        }
        float[] latLong = new float[2];
        boolean hasLatLong = exif.getLatLong(latLong);
        if (hasLatLong) {
            latlon.add(0, "" + latLong[0]);
            latlon.add(1, "" + latLong[1]);
        }
        else
        {
            latlon.add(0, " (unknown) ");
            latlon.add(1, " (unknown) ");
        }

        return latlon;
    }


}
