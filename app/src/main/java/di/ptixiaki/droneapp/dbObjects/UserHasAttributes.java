package di.ptixiaki.droneapp.dbObjects;

import java.util.ArrayList;

/**
 * Created by sinnaig on 4/5/2015.
 */
public class UserHasAttributes {

    private String user = new String();
    private ArrayList<String> attributesList = new ArrayList<String>();

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }


    public void addAttribute(String attribute) {
        this.attributesList.add(attribute);
    }

    public ArrayList<String> getAttributesList() {
        return attributesList;
    }

}
