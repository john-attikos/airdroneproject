package di.ptixiaki.droneapp.storage;


import android.os.Environment;
import android.util.Log;

import java.io.File;

import di.ptixiaki.droneapp.debug.MyLogger;

public class IntStorage {

    public static String imagesDir = "/DroneAppImages";
    public static String logosDir = "/DroneAppLogos";
    public static String appImagesPath = Environment.getExternalStorageDirectory() + imagesDir + "/";

    public static void createAppDir() {

        File file = new File(Environment.getExternalStorageDirectory(), imagesDir);

        if ( !file.exists() )
        {
            if ( !file.mkdir() )
                MyLogger.printe("Problem creating Drone folder");
            else
                MyLogger.printd(imagesDir +" folder created! in: "+file.getAbsolutePath());
        }
    }

    public static void createUserAppDir(String user) {

        File file = new File(Environment.getExternalStorageDirectory(), imagesDir + "/" + user);

        if ( !file.exists() )
        {
            if ( !file.mkdir() )
                MyLogger.printe("Problem creating user's Drone folder for images");
            else
                MyLogger.printd(imagesDir + "/" + user +" folder created! in: "+file.getAbsolutePath());
        }
    }

    public static String getDirPathForImages()
    {
        File sdCardRoot = Environment.getExternalStorageDirectory();

        return sdCardRoot+""+imagesDir;
    }

    public static String getDirPathForLogos()
    {
        File sdCardRoot = Environment.getExternalStorageDirectory();

        return sdCardRoot+""+ logosDir;
    }

    // For debugging purposes
    public static void printDroneDirContentsForImages() {
        File sdCardRoot = Environment.getExternalStorageDirectory();
        File droneDir = new File(sdCardRoot, imagesDir);
        for (File f : droneDir.listFiles())
            Log.d("+++", f.getName());
    }

    // For debugging purposes
    public static void printDroneDirContentsForLogos() {
        File sdCardRoot = Environment.getExternalStorageDirectory();
        File droneDir = new File(sdCardRoot, logosDir);
        for (File f : droneDir.listFiles())
            Log.d("+++", f.getName());
    }

}
