package di.ptixiaki.droneapp.debug;

/**
 * Created by sinnaig on 19/3/2016.
 */
public class MyLogger {

    ////////////////////////////////////////////////////////////////
    // Adding the "+++" pattern to monitor only my logs in logcat //
    ////////////////////////////////////////////////////////////////

    private static final String debug = "+++[DEBUG] ";
    private static final String info = "+++[INFO] ";
    private static final String warn = "+++[WARN] ";
    private static final String error = "+++[ERROR] ";

    public static void printd(String message)
    {
        System.out.println(debug + message);
    }

    public static void printi(String message)
    {
        System.out.println(info + message);
    }

    public static void printw(String message)
    {
        System.out.println(warn + message);
    }

    public static void printe(String message) { System.out.println(error + message); }

}