package di.ptixiaki.droneapp.imagePresentation;


import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.text.format.DateFormat;

import java.io.File;
import java.sql.Date;
import java.util.ArrayList;

import di.ptixiaki.droneapp.dbObjects.Image;

public class ImageProperties {


    String name = new String();
    String date = new String();
    String path = new String();
    String description = new String();
    long dateSecs;
    int size;
    int width;
    int height;
    public static boolean imageListIsCreated = false;

    public long getDateSecs() {
        return dateSecs;
    }

    public void setDateSecs(long dateSecs) {
        this.dateSecs = dateSecs;
    }

    public String getPath() {
        return this.path;
    }
    public void setPath(String path) {
        this.path = path;
    }


    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }


    public int getWidth() {
        return width;
    }
    public void setWidth(int width) {
        this.width = width;
    }

    public String getDate() {
        return date;
    }

    public String getName() {
        return name;
    }

    public int getHeight() {
        return height;
    }

    public String getDescription() {
        return description;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setName(String name) {
        this.name = name;
    }


    public static ArrayList<File> sortImagesByDate(ArrayList<File> images) {
        ArrayList<File> sortedFiles = new ArrayList<File>();

        while (!images.isEmpty()) {
            File minDateFile = images.get(0);
            int minDatePos = 0;
            for (int i=0; i<images.size(); i++) {
                if (images.get(i).lastModified() < minDateFile.lastModified()) {
                    minDateFile = images.get(i);
                    minDatePos = i;
                }
            }
            sortedFiles.add(minDateFile);
            images.remove(minDatePos);
        }

        return sortedFiles;
    }

    //////////////////////////////////////////////
    // returns image details in a single String //
    public static String getStringImageDetails(String path) {
        StringBuilder details = new StringBuilder();
        File file = new File(path);
        Bitmap bm = null;
        // First decode with inJustDecodeBounds=true to check dimensions
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(path, options);
        bm = BitmapFactory.decodeFile(path);

        String longV = Long.toString(file.lastModified());
        long secsFrom1970 = Long.parseLong(longV);
        String dateString= DateFormat.format("MM/dd/yyyy", new Date(secsFrom1970)).toString();

        ArrayList<String> latlon = Image.getImageLatLon(path);

        details.append("Name: "+file.getName());
        details.append("\nDate: "+dateString);
        details.append("\nHeight: "+bm.getHeight()+" px");
        details.append("\nWidth: "+bm.getWidth()+" px");
        //convert bytes to kilobytes
        float kilos = (bm.getRowBytes()*bm.getHeight()) / 1024;
        details.append("\nSize: "+kilos+" KB");
        details.append("\n\nLATITUDE: " + latlon.get(0));
        details.append("\nLONGITUDE: "  + latlon.get(1));

        return details.toString();
    }

}
