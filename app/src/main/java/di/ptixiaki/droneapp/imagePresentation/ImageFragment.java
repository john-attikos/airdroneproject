package di.ptixiaki.droneapp.imagePresentation;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.Fragment;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.sql.Date;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.concurrent.ExecutionException;

import di.ptixiaki.droneapp.R;
import di.ptixiaki.droneapp.debug.MyLogger;
import di.ptixiaki.droneapp.main.LoginActivity;
import di.ptixiaki.droneapp.main.Network;
import di.ptixiaki.droneapp.storage.IntStorage;
import di.ptixiaki.droneapp.webServices.GetDroneImages;

public class ImageFragment extends Fragment {

    //we keep in lists images names and images properties
    public ArrayList<ImageProperties> imagesPropertiesList = new ArrayList<ImageProperties>();
    public ArrayList<String> imagesNameList = new ArrayList<String>();
    public int selectedItemPos;
    Fragment currentFragment = this;

    public class ImageAdapter extends BaseAdapter {

        private Context mContext;
        ArrayList<String> itemsList = new ArrayList<String>();

        public ImageAdapter(Context c)
        {
            mContext = c;
        }

        void add(String path)
        {
            itemsList.add(path);

            Collections.sort(imagesPropertiesList, new Comparator<ImageProperties>() {
                @Override public int compare(ImageProperties p1, ImageProperties p2) {
                    return ((int) p1.getDateSecs())- ((int) p2.getDateSecs());
                }
            });
        }

        @Override
        public int getCount() {
            return itemsList.size();
        }

        @Override
        public Object getItem(int i) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            ImageView imageView;
            if (convertView == null) {
                imageView = new ImageView(mContext);
                imageView.setLayoutParams(new GridView.LayoutParams(150, 150));
                imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
                imageView.setPadding(8, 8, 8, 8);
            } else {
                imageView = (ImageView) convertView;
            }

            Bitmap bm = renderImage(itemsList.get(position), 150, 150);
            imageView.setImageBitmap(bm);

            return imageView;
        }


        public Bitmap renderImage(String path, int reqWidth, int reqHeight) {

            Bitmap bm = null;
            // First decode with inJustDecodeBounds=true to check dimensions
            final BitmapFactory.Options options = new BitmapFactory.Options();
            options.inJustDecodeBounds = true;
            BitmapFactory.decodeFile(path, options);

            // Calculate inSampleSize
            options.inSampleSize = calculateImageSize(options, reqWidth, reqHeight);

            // Decode bitmap with inSampleSize set
            options.inJustDecodeBounds = false;
            bm = BitmapFactory.decodeFile(path, options);

            File file = new File(path);

            //every time we recreate the fragment we check only for images
            //that our list does not have
            if (!imagesNameList.contains(file.getName())) {

                //converting secs from 1970 to date(string)
                String longV = Long.toString(file.lastModified());
                long secsFrom1970 = Long.parseLong(longV);
                String dateString= DateFormat.format("MM/dd/yyyy", new Date(secsFrom1970)).toString();

                ImageProperties image = new ImageProperties();
                image.setSize(bm.getRowBytes() * bm.getHeight());
                image.setWidth(bm.getWidth());
                image.setHeight(bm.getHeight());
                image.setDate(dateString);
                image.setName(file.getName());
                image.setPath(file.getAbsolutePath());

                imagesPropertiesList.add(image);
                imagesNameList.add(file.getName());
            }

            return bm;
        }

        public int calculateImageSize(

            BitmapFactory.Options options, int reqWidth, int reqHeight) {
            // Raw height and width of image
            final int height = options.outHeight;
            final int width = options.outWidth;
            int inSampleSize = 1;


            if (height > reqHeight || width > reqWidth) {
                if (width > height) {
                    inSampleSize = Math.round((float)height / (float)reqHeight);
                } else {
                    inSampleSize = Math.round((float)width / (float)reqWidth);
                }
            }
            return inSampleSize;
        }
    }


    ImageAdapter myImageAdapter;

    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_image, container, false);

        final GridView gridView = (GridView) rootView.findViewById(R.id.gridview);
        myImageAdapter = new ImageAdapter(this.getActivity());

        final Button downloadButton = (Button) rootView.findViewById(R.id.image_download_button);
        downloadButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Boolean network = Network.haveNetworkConnection(LoginActivity.context);
                if (!network)
                {
                    Toast.makeText(getActivity().getBaseContext(), "Something is wrong with the connection...", Toast.LENGTH_LONG).show();
                    return;
                }
                final ProgressDialog progress;
                progress = ProgressDialog.show(getActivity(), "Downloading your Drone images",
                        "Please wait...", true);

                Thread downloadThread = new Thread(new Runnable() {
                    @Override
                    public void run()
                    {
                        downloadImages();
                        progress.dismiss();
                    }
                });
                downloadThread.start();

            }

        });

        setRetainInstance(true);

        gridView.setAdapter(myImageAdapter);

        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int itemPos, long l) {
                selectedItemPos = itemPos;
                imagePresentation(getView());
            }
        });

        String ExternalStorageDirectoryPath = Environment
                .getExternalStorageDirectory()
                .getAbsolutePath();

        String targetPath = ExternalStorageDirectoryPath + IntStorage.imagesDir + "/" + LoginActivity.user;
        File targetDirector = new File(targetPath);


        File[] files = targetDirector.listFiles();

        TextView text;
        text = (TextView) rootView.findViewById(R.id.image_tab_text);
        if (files.length == 0) {
            text.setText("Oups!...\nSeems like you don't have any Drone photos yet!");
        }
        else
            text.setVisibility(View.GONE);

        //first lets sort those images by date
        ArrayList<File> unsortedImages = new ArrayList<File>(Arrays.asList(files));
        for (int i=0; i<unsortedImages.size(); i++) {
            String filePath = unsortedImages.get(i).getPath();
            if (filePath.endsWith(".jpg")   || filePath.endsWith(".png")
                                            || filePath.endsWith(".gif")
                                            || filePath.endsWith(".bmp")
                                            || filePath.endsWith(".WebP")
                                            || filePath.endsWith(".jpeg"));
            else //removing non image format files from list
                unsortedImages.remove(i);
        }

        //sorting our files by date
        List<File> sortedImages = ImageProperties.sortImagesByDate(unsortedImages);

        for (int i=0; i<sortedImages.size(); i++) {
            myImageAdapter.add(sortedImages.get(i).getAbsolutePath());
        }
        return rootView;
    }


    public Boolean downloadImages() {

        Boolean network = false;
        try {
            network = new GetDroneImages().execute().get();
            if (!network) {
                Toast.makeText(getActivity().getBaseContext(), "Something is wrong with the connection...", Toast.LENGTH_LONG).show();
                return network;
            }
        } catch (InterruptedException e) {
            MyLogger.printe("Something went wrong with the Drone images download...");
            e.printStackTrace();
        } catch (ExecutionException e) {
            MyLogger.printe("Something went wrong with the Drone images download...");
            e.printStackTrace();
        }

        getFragmentManager()
            .beginTransaction()
            .detach(currentFragment)
            .attach(currentFragment)
            .commit();

        return network;
    }



    public void imagePresentation(View view) {

        Intent intent = new Intent(getActivity(), ImagePresentation.class);
        String imageName = imagesPropertiesList.get(selectedItemPos).getName();
        String imagePath = IntStorage.getDirPathForImages()+ "/" + LoginActivity.user + "/" + imageName;
        intent.putExtra("imagePath", imagePath);
        intent.putExtra("imageName", imageName);
        startActivity(intent);
    }

}
